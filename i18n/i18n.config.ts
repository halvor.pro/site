import { en, es } from "vuetify/locale";

export default defineI18nConfig(() => ({
  legacy: false,
  messages: { en: { $vuetify: en }, es: { $vuetify: es } },
  fallbackLocale: "en"
}));
