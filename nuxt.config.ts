// https://nuxt.com/docs/api/configuration/nuxt-config
import vuetify from "vite-plugin-vuetify";

// noinspection JSUnusedGlobalSymbols
export default defineNuxtConfig({
  app: {
    head: {
      charset: "utf-8",
      viewport: "width=device-width, initial-scale=1.0",
      meta: [
        {
          name: "site_name", content: "Halvor Pro"
        },
        {
          name: "twitter:card", content: "summary"
        },
        {
          name: "twitter:site", content: "https://halvor.pro"
        },
        {
          name: "twitter:image", content: "https://halvor.pro/android-chrome-512x512.png"
        },
        {
          name: "og:image", content: "https://halvor.pro/android-chrome-512x512.png"
        },
        {
          name: "author", content: "Halvor Stephen Peña Madrigal"
        },
        {
          name: "msapplication-TileColor", content: "#F7F7F9"
        },
        {
          name: "theme-color", content: "#F7F7F9", media: "(prefers-color-scheme: light)"
        },
        {
          name: "theme-color", content: "#121212", media: "(prefers-color-scheme: dark)"
        },
        {
          name: "background-color", content: "#F7F7F9", media: "(prefers-color-scheme: light)"
        },
        {
          name: "background-color", content: "#121212", media: "(prefers-color-scheme: dark)"
        },
        {
          name: "apple-mobile-web-app-status-bar-style", content: "black-translucent"
        }
      ],
      link: [
        {
          rel: "apple-touch-icon", sizes: "180x180", href: "/apple-touch-icon.png"
        },
        {
          rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png"
        },
        {
          rel: "shortcut icon", href: "/favicon.ico"
        },
        {
          rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png"
        },
        {
          rel: "manifest", href: "/site.webmanifest"
        },
        {
          rel: "mask-icon", href: "/safari-pinned-tab.svg", color: "#303030"
        }
      ]
    }
  },
  build: {
    transpile: ["vuetify"]
  },
  css: [
    "~/assets/main.scss",
    "~/assets/styles.scss"
  ],
  compatibilityDate: "2024-04-03",
  devtools: { enabled: true },
  modules: [
    (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        config.plugins?.push(vuetify({
          styles: {
            configFile: "assets/settings.scss"
          }
        }));
      });
    },
    "@nuxt/eslint",
    "@nuxt/fonts",
    "@vueuse/nuxt",
    "@nuxtjs/i18n",
    "@nuxtjs/sitemap",
    "@nuxtjs/robots",
    "nuxt-gtag",
    "@nuxtjs/tailwindcss"
  ],
  gtag: {
    id: "G-VGC4GFQZ64",
    initCommands: [
      // Setup up consent mode
      ["consent", "default", {
        ad_user_data: "denied",
        ad_personalization: "denied",
        ad_storage: "denied"
      }]
    ]
  },
  i18n: {
    baseUrl: "https://halvor.pro",
    locales: [{ code: "en", language: "en" }, { code: "es", language: "es" }],
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "i18n_locale",
      redirectOn: "root"
    },
    defaultLocale: "en",
    strategy: "prefix_and_default"
  },
  robots: {
    allow: "/"
  },
  nitro: {
    compressPublicAssets: true,
    prerender: {
      crawlLinks: true
    }
  },
  site: {
    url: "https://halvor.pro"
  },
  ssr: true,
  sourcemap: false,
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          api: "modern"
        }
      }
    }
  }
});
