import type { Config } from "tailwindcss";
export default {
  content: [],
  darkMode: "selector",
  theme: {
    fontFamily: {
      sans: "Inter, sans-serif"
    }
  },
  plugins: []
} satisfies Config;
