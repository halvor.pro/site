import type { ThemeDefinition } from "vuetify";
import { createVuetify } from "vuetify";
import { md3 } from "vuetify/blueprints";
import { VBtn, VContainer } from "vuetify/components";
import { createVueI18nAdapter } from "vuetify/locale/adapters/vue-i18n";
import aliases from "~/iconsets";
import stroke from "~/iconsets/stroke";

const palette = {
  primary: "#9E77ED",
  "on-primary": "#FFFFFF",
  "primary-darken": "#875BF7",
  "on-primary-darken": "#FFFFFF",
  "primary-lighten": "#D6BBFB",
  "on-primary-lighten": "#000000",

  secondary: "#528BFF",
  "on-secondary": "#FFFFFF",
  "secondary-darken": "#175CD3",
  "on-secondary-darken": "#FFFFFF",
  "secondary-lighten": "#B2CCFF",
  "on-secondary-lighten": "#000000"
};

const light: ThemeDefinition = {
  dark: false,
  colors: {
    ...palette,
    background: "#F7F7F9",
    "on-background": "#4C4E64DE",
    surface: "#FFFFFF",
    "on-surface": "#4C4E6438",
    error: "#F04438",
    "on-error": "#FFFFFF",
    warning: "#F79009",
    "on-warning": "#FFFFFF",
    success: "#17B26A",
    "on-success": "#FFFFFF",
    info: "#2970FF",
    "on-info": "#FFFFFF"
  }
};

const dark: ThemeDefinition = {
  dark: true,
  colors: {
    ...palette,
    background: "#121212",
    "on-background": "#F2F2F2",
    surface: "#202124",
    "on-surface": "#F8FAFC",
    "surface-variant": "#303030",
    "on-surface-variant": "#F8FAFC",
    error: "#F97066",
    "on-error": "#000000",
    warning: "#FEC84B",
    "on-warning": "#121212",
    info: "#528BFF",
    "on-info": "#000000"
  }
};

// noinspection JSUnusedGlobalSymbols
export default defineNuxtPlugin((app) => {
  const i18n = { global: app.$i18n };
  const theme = useCookie<"light" | "dark">("theme", {
    default: () => "light"
  });
  const vuetify = createVuetify({
    aliases: {
      VFab: VBtn,
      VPage: VContainer
    },
    blueprint: md3,
    defaults: {
      VAlert: {
        variant: "tonal",
        rounded: "lg"
      },
      VAppBarNavIcon: {
        rounded: "xl"
      },
      VBtn: {
        color: "secondary",
        rounded: "lg",
        exact: true,
        class: ["px-4"]
      },
      VCardTitle: {
        class: ["text-wrap"]
      },
      VCheckbox: {
        color: "secondary",
        density: "comfortable",
        hideDetails: true
      },
      VCheckboxBtn: {
        color: "secondary"
      },
      VChip: {
        rounded: "rounded"
      },
      VCol: {
        cols: 12
      },
      VFab: {
        color: "primary",
        position: "fixed",
        location: "bottom end",
        class: ["mb-10", "mr-4"]
      },
      VForm: {
        validateOn: "blur"
      },
      VFileInput: {
        color: "primary",
        rounded: "lg",
        variant: "outlined",
        prependIcon: "",
        appendInnerIcon: "file"
      },
      VListSubheader: {
        class: ["text-uppercase", "font-bold"]
      },
      VList: {
        color: "primary"
      },
      VListItem: {
        rounded: "lg"
      },
      VListItemTitle: {
        class: "font-weight-medium"
      },
      VTextarea: {
        variant: "outlined",
        rounded: "lg",
        color: "primary"
      },
      VTextField: {
        variant: "outlined",
        rounded: "lg",
        color: "primary"
      },
      VPage: {
        fluid: true,
        class: ["flex", "grow", "flex-col"]
      },
      VProgressCircular: {
        indeterminate: true,
        color: "secondary",
        ariaLabel: "$vuetify.loading"
      },
      VProgressLinear: {
        indeterminate: true,
        rounded: "rounded",
        color: "secondary",
        ariaLabel: "$vuetify.loading"
      },
      VSelect: {
        variant: "outlined",
        rounded: "lg",
        color: "primary",
        title: ""
      }
    },
    icons: {
      defaultSet: "stroke",
      aliases,
      sets: {
        stroke
      }
    },
    theme: {
      defaultTheme: theme.value,
      themes: { light, dark }
    },
    locale: {
      // @ts-expect-error - Adapter does not export types
      adapter: createVueI18nAdapter({ i18n, useI18n })
    },
    ssr: true
  });

  app.vueApp.use(vuetify);
});
